# blazorToAmplify

This repository holds the demo code for following the workshop here: [Deploy .NET Blazor WebAssembly Application to AWS Amplify](https://davidconoh.hashnode.dev/deploy-net-blazor-webassembly-application-to-aws-amplify/). Check it out!

Other workable hands-on lab by the same author: [Hosting Uno Platform WASM on AWS Amplify](https://davidconoh.hashnode.dev/hosting-uno-platform-wasm-on-aws-amplify/). Ready to read?! 🧊🎉
